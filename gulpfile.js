'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./hackathon_starter/hackathon/static/sass/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./hackathon_starter/hackathon/static/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./hackathon_starter/hackathon/static/sass/*.scss', ['sass']);
});
